module codeberg.org/gruf/go-afwm

go 1.21.0

toolchain go1.23.2

require (
	codeberg.org/gruf/go-byteutil v1.3.0
	codeberg.org/gruf/go-fflag v0.0.0-20240113192453-f29e0bcdcbee
	codeberg.org/gruf/go-fsys v0.0.0-20230508183124-d1809af32b35
	codeberg.org/gruf/go-kv v1.6.5
	codeberg.org/gruf/go-logger/v4 v4.0.5
	codeberg.org/gruf/go-split v1.1.0
	codeberg.org/gruf/go-xgb v1.2.2
	github.com/pelletier/go-toml/v2 v2.2.3
	golang.org/x/sys v0.29.0
)

require (
	codeberg.org/gruf/go-bytesize v1.0.3 // indirect
	golang.org/x/exp v0.0.0-20240119083558-1b970713d09a // indirect
)

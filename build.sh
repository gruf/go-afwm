#!/bin/sh

CGO_ENABLED=0 \
go build -trimpath -v \
         -tags 'netgo osusergo static_build kvformat' \
         -ldflags '-s -w' \
         -gcflags=all='-l=4' \
         -o afwm ./cmd/afwm/

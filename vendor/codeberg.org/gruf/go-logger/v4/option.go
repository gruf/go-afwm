package logger

import (
	"io"
	"sync"
)

type Option interface {
	Apply(*Logger)
}

type optionfunc func(*Logger)

func (fn optionfunc) Apply(logger *Logger) {
	fn(logger)
}

func WithOutput(out io.Writer) Option {
	return optionfunc(func(l *Logger) {
		if out == nil {
			l.out = io.Discard
			return
		}
		l.out = out
	})
}

func WithSafeOutput(out io.Writer) Option {
	return optionfunc(func(l *Logger) {
		if out == nil {
			l.out = io.Discard
			return
		}
		l.out = &safeWriter{Writer: out}
	})
}

func WithLevel(lvl LEVEL) Option {
	return optionfunc(func(l *Logger) {
		l.store(lvl, l.Flags())
	})
}

func WithTimestamp(enabled bool) Option {
	return optionfunc(func(l *Logger) {
		flags := l.Flags()
		if enabled {
			flags |= LTimestamp
		} else {
			flags &= ^LTimestamp
		}
		l.SetFlags(flags)
	})
}

func WithCaller(enabled bool) Option {
	return optionfunc(func(l *Logger) {
		flags := l.Flags()
		if enabled {
			flags |= LCaller
		} else {
			flags &= ^LCaller
		}
		l.SetFlags(flags)
	})
}

func WithClock(now func() string) Option {
	return optionfunc(func(l *Logger) {
		l.now = now
	})
}

type safeWriter struct {
	io.Writer
	sync.Mutex
}

func (w *safeWriter) Write(b []byte) (int, error) {
	w.Lock()
	defer w.Unlock()
	return w.Writer.Write(b)
}

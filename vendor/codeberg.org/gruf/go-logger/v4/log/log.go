package log

import (
	"fmt"
	"os"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-kv"
	"codeberg.org/gruf/go-logger/v4"
)

var Logger = logger.New(
	logger.WithSafeOutput(os.Stderr),
	logger.WithLevel(logger.ALL),
	logger.WithTimestamp(true),
	logger.WithCaller(false),
)

func Entry() logger.Entry {
	return logger.Entry{
		Lvl:  logger.UNSET,
		Data: nil,
		Msg:  "",
		Out:  Logger,
	}
}

func Trace(args ...interface{}) {
	Logger.Write(2, logger.TRACE, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprint(args...))
	})
}

func Tracef(format string, args ...interface{}) {
	Logger.Write(2, logger.TRACE, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprintf(format, args...))
	})
}

func TraceKV(key string, value any) {
	Logger.Write(2, logger.TRACE, func(buf *byteutil.Buffer) {
		kv.Field{K: key, V: value}.AppendFormat(buf, false)
	})
}

func TraceKVs(fields ...kv.Field) {
	Logger.Write(2, logger.TRACE, func(buf *byteutil.Buffer) {
		kv.Fields(fields).AppendFormat(buf, false)
	})
}

func Debug(args ...interface{}) {
	Logger.Write(2, logger.DEBUG, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprint(args...))
	})
}

func Debugf(format string, args ...interface{}) {
	Logger.Write(2, logger.DEBUG, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprintf(format, args...))
	})
}

func DebugKV(key string, value any) {
	Logger.Write(2, logger.DEBUG, func(buf *byteutil.Buffer) {
		kv.Field{K: key, V: value}.AppendFormat(buf, false)
	})
}

func DebugKVs(fields ...kv.Field) {
	Logger.Write(2, logger.DEBUG, func(buf *byteutil.Buffer) {
		kv.Fields(fields).AppendFormat(buf, false)
	})
}

func Info(args ...interface{}) {
	Logger.Write(2, logger.INFO, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprint(args...))
	})
}

func Infof(format string, args ...interface{}) {
	Logger.Write(2, logger.INFO, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprintf(format, args...))
	})
}

func InfoKV(key string, value any) {
	Logger.Write(2, logger.INFO, func(buf *byteutil.Buffer) {
		kv.Field{K: key, V: value}.AppendFormat(buf, false)
	})
}

func InfoKVs(fields ...kv.Field) {
	Logger.Write(2, logger.INFO, func(buf *byteutil.Buffer) {
		kv.Fields(fields).AppendFormat(buf, false)
	})
}

func Warn(args ...interface{}) {
	Logger.Write(2, logger.WARN, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprint(args...))
	})
}

func Warnf(format string, args ...interface{}) {
	Logger.Write(2, logger.WARN, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprintf(format, args...))
	})
}

func WarnKV(key string, value any) {
	Logger.Write(2, logger.WARN, func(buf *byteutil.Buffer) {
		kv.Field{K: key, V: value}.AppendFormat(buf, false)
	})
}

func WarnKVs(fields ...kv.Field) {
	Logger.Write(2, logger.WARN, func(buf *byteutil.Buffer) {
		kv.Fields(fields).AppendFormat(buf, false)
	})
}

func Error(args ...interface{}) {
	Logger.Write(2, logger.ERROR, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprint(args...))
	})
}

func Errorf(format string, args ...interface{}) {
	Logger.Write(2, logger.ERROR, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprintf(format, args...))
	})
}

func ErrorKV(key string, value any) {
	Logger.Write(2, logger.ERROR, func(buf *byteutil.Buffer) {
		kv.Field{K: key, V: value}.AppendFormat(buf, false)
	})
}

func ErrorKVs(fields ...kv.Field) {
	Logger.Write(2, logger.ERROR, func(buf *byteutil.Buffer) {
		kv.Fields(fields).AppendFormat(buf, false)
	})
}

func Panic(args ...interface{}) {
	str := fmt.Sprint(args...)
	Logger.Write(2, logger.PANIC, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, str)
	})
	panic(str)
}

func Panicf(format string, args ...interface{}) {
	str := fmt.Sprintf(format, args...)
	Logger.Write(2, logger.PANIC, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, str)
	})
	panic(str)
}

func PanicKV(key string, value any) {
	str := kv.Field{K: key, V: value}.String()
	Logger.Write(2, logger.PANIC, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, str...)
	})
	panic(str)
}

func PanicKVs(fields ...kv.Field) {
	str := kv.Fields(fields).String()
	Logger.Write(2, logger.PANIC, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, str...)
	})
	panic(str)
}

func Print(args ...interface{}) {
	Logger.Write(2, logger.UNSET, func(buf *byteutil.Buffer) {
		fmt.Fprint(buf, args...)
	})
}

func Printf(format string, args ...interface{}) {
	Logger.Write(2, logger.UNSET, func(buf *byteutil.Buffer) {
		fmt.Fprintf(buf, format, args...)
	})
}

func PrintKV(key string, value any) {
	Logger.Write(2, logger.UNSET, func(buf *byteutil.Buffer) {
		kv.Field{K: key, V: value}.AppendFormat(buf, false)
	})
}

func PrintKVs(fields ...kv.Field) {
	Logger.Write(2, logger.UNSET, func(buf *byteutil.Buffer) {
		kv.Fields(fields).AppendFormat(buf, false)
	})
}

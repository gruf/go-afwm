# afwm

Super simple X window manager in Go.

Built around my own rewrite of `go-xgb`.

No *full* ICCCM/EWMH support planned, just enough to get by. As for why, see: https://raw.githubusercontent.com/kfish/xsel/master/rant.txt (backup in `docs/xsel_rant.txt`)

Floating only.

Keepin' it simple.

```sh
Usage: afwm ...
 -c --config string
        Configuration file path
 -d --display string (env: $DISPLAY)
        X display address
 -h --help
        Print usage information
 -v --version
        Print version information
```

![screenshot](https://codeberg.org/gruf/go-afwm/raw/main/screenshot.png)

Example config:
```toml
# Screen padding (px)
screen-pad = 5

# Min window dimensions (px)
min-width = 480
min-height = 240

# Mouse modifier key
mouse-mod = "super"

# Keyboard bindings
key-binds = [
    'super + p = exec "dmenu_run -fn 'Ubuntu Mono:size=12' -nb '#1d1f21' -nf '#66d9ef' -sb '#1d1f21' -sf '#66d9ef'"',
    'bright_up = exec "xbacklight -inc 5"',
    'bright_down = exec "xbacklight -dec 5"',
    'audio_up = exec "amixer sset Master 5%+"',
    'audio_down = exec "amixer sset Master 5%-"',
    'audio_mute = exec "amixer sset Master 1+ toggle"',
    'mic_mute = exec "amixer sset Capture 1+ toggle"',
    'printscrn = screenshot "."',
    'shift + printscrn = capture "."',
    'super + shift + enter = exec "alacritty"',
    'super + shift + c = close',
    'super + shift + q = quit',
    'super + f = toggleFullscreen',
    'super + l = exec "xsecurelock"',
    'super + 1 = goToWorkspace 1',
    'super + 2 = goToWorkspace 2',
    'super + 3 = goToWorkspace 3',
    'super + 4 = goToWorkspace 4',
    'super + 5 = goToWorkspace 5',
    'super + 6 = goToWorkspace 6',
    'super + 7 = goToWorkspace 7',
    'super + 8 = goToWorkspace 8',
    'super + 9 = goToWorkspace 9',
    'super + shift + 1 = sendToWorkspace 1',
    'super + shift + 2 = sendToWorkspace 2',
    'super + shift + 3 = sendToWorkspace 3',
    'super + shift + 4 = sendToWorkspace 4',
    'super + shift + 5 = sendToWorkspace 5',
    'super + shift + 6 = sendToWorkspace 6',
    'super + shift + 7 = sendToWorkspace 7',
    'super + shift + 8 = sendToWorkspace 8',
    'super + shift + 9 = sendToWorkspace 9',
    'alt + up = move 0 -1',
    'alt + down = move 0 +1',
    'alt + left = move -1 0',
    'alt + right = move +1 0',
    'alt + shift + up = resize 0 -1',
    'alt + shift + down = resize 0 +1',
    'alt + shift + left = resize -1 0',
    'alt + shift + right = resize +1 0',
    'super + up = move 0 -10',
    'super + down = move 0 +10',
    'super + left = move -10 0',
    'super + right = move +10 0',
    'super + shift + up = resize 0 -10',
    'super + shift + down = resize 0 +10',
    'super + shift + left = resize -10 0',
    'super + shift + right = resize +10 0',
]
```

Example xinitrc:
```sh
#!/bin/sh

(while true; do echo "$(date +%T)"; sleep 1; done) | lemonbar -f ' - 12' &

exec afwm
```

# Building

Running `./build.sh` will build a static binary for your current build target.

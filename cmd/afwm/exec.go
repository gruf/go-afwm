package main

import (
	"os/signal"
	"path"
	"strings"
	"syscall"

	"codeberg.org/gruf/go-afwm/wm"
)

func init() {
	// Ignore all SIGCHLD signals, this means that
	// we do not have to perform wait4() on each PID
	// after forkexec() to check the status of it, lest
	// we leave zombie processes everywhere... we can
	// just leave it up to init to cleanup
	signal.Ignore(syscall.SIGCHLD)
}

// environ is our own copy of the executing environment.
var environ = syscall.Environ()

// shell contains the determined shell binary to use.
var shell = func() string {
	for _, kv := range environ {
		if strings.HasPrefix(kv, "SHELL=") {
			return kv[6:]
		}
	}
	return "/bin/sh"
}()

// dir contains the determined current working directory.
var dir = func() string {
	for _, kv := range environ {
		if strings.HasPrefix(kv, "PWD=") {
			return kv[4:]
		}
	}
	wd, _ := syscall.Getenv("PWD")
	return wd
}()

// procAttr contains predetermined ProcAttr for forkexec syscalls.
var procAttr = syscall.ProcAttr{
	Dir: dir,
	Env: environ,
	Files: []uintptr{
		uintptr(syscall.Stdin),
		uintptr(syscall.Stdout),
		uintptr(syscall.Stderr),
	},
}

// shexec will return a keybind function capable of executing provided shell cmd.
func shexec(args string) func(*wm.WM, *wm.Window) error {
	return exec(shell, "-c", args)
}

// exec will return a keybind function capable of executing provided executable and arguments.
func exec(exec string, args ...string) func(*wm.WM, *wm.Window) error {
	args = append([]string{path.Base(exec)}, args...)
	return func(*wm.WM, *wm.Window) error {
		return forkexec(exec, args)
	}
}

// forkexec will forkexec provided args.
func forkexec(argv0 string, args []string) error {
	_, err := syscall.ForkExec(argv0, args, &procAttr)
	return err
}

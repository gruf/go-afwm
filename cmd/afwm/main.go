package main

import (
	"os"
	"os/signal"
	"strings"
	"syscall"

	"codeberg.org/gruf/go-afwm/wm"
	"codeberg.org/gruf/go-fflag"
	"codeberg.org/gruf/go-fsys"
	"codeberg.org/gruf/go-logger/v4"
	"codeberg.org/gruf/go-logger/v4/log"
	"codeberg.org/gruf/go-split"
	"github.com/pelletier/go-toml/v2"
	"golang.org/x/sys/unix"
)

func main() {
	var (
		// exit code.
		code int

		// configuration vars.
		config = struct {
			Display   string   `toml:"-" short:"d" long:"display" usage:"X display address" required:"true"`
			LogLevel  string   `toml:"log-level"`
			ScreenPad uint     `toml:"screen-pad"`
			MinWidth  uint     `toml:"min-width"`
			MinHeight uint     `toml:"min-height"`
			KeyBinds  []string `toml:"key-binds"`
			MouseMod  string   `toml:"mouse-mod"`
		}{
			// defaults values.
			LogLevel:  "info",
			ScreenPad: 5,
			MinWidth:  240,
			MinHeight: 480,
			MouseMod:  "alt",
		}

		// window manager.
		wm wm.WM
	)

	// Standard out writer.
	out := fsys.Stdout()

	// Get usable default keybind slice for CLI from defaults text.
	config.KeyBinds, _ = split.SplitStrings[string](DefaultKeyBinds)

	// Setup flags
	fflag.Help()
	fflag.AutoEnv(nil)
	fflag.StructFields(&config)
	fflag.Config(func(file *os.File) error {
		return toml.NewDecoder(file).
			DisallowUnknownFields().
			Decode(&config)
	})

	// Parse runtime flags, print usage on unknowns
	if extra := fflag.MustParse(); len(extra) > 0 {
		var usage string
		usage += "Usage: " + os.Args[0] + " ...\n" + fflag.Usage() + "\n"
		usage += "unexpected cli args: " + strings.Join(extra, " ") + "\n"
		out.WriteString(usage)
		unix.Exit(1)
	}

	// Parse configuration from CLI variables.
	wm.Config.KeyBinds = parseKeyBinds(config.KeyBinds)
	wm.Config.MinWindowWidth = uint32(config.MinWidth)
	wm.Config.MinWindowHeight = uint32(config.MinHeight)
	wm.Config.ModKey = parseMods(config.MouseMod)
	wm.Config.ScreenPadding = uint32(config.ScreenPad)

	// Set global logger preferences.
	log.Logger.SetOutput(out)
	log.Logger.SetFlags(logger.LCaller)
	logLvl, err := logger.ParseLevel(config.LogLevel)
	if err != nil {
		log.Panic(err)
	}
	log.Logger.SetLevel(logLvl + 1)

	// Attempt to initialize wm object with addr.
	if err := wm.Init(config.Display); err != nil {
		log.Panic(err)
	}

	// Register for OS signals.
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGTERM, syscall.SIGINT)

	go func() {
		log.Infof("Signal %s received", <-sigs)
		wm.Stop() // shut down window manager
	}()

	// Start the window manager.
	if err := wm.Run(); err != nil {
		log.Error(err)
		code = 1
	}

	// Exit with code.
	unix.Exit(code)
}

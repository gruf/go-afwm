package main

import (
	"fmt"
	"image/png"
	"strings"
	"time"
	"unicode"

	"codeberg.org/gruf/go-afwm/wm"
	"codeberg.org/gruf/go-fsys"
	"golang.org/x/sys/unix"
)

// Screenshot time format string used in output file name.
const ScreenshotFmt = "screenshot_2006_01_02_15_04_05.png"

// Default keybinds CLI string input.
const DefaultKeyBinds = `
super + p = exec "dmenu_run",
bright_up = exec "xbacklight -inc 5",
bright_down = exec "xbacklight -dec 5",
audio_up = exec "amixer sset Master 5%+",
audio_down = exec "amixer sset Master 5%-",
audio_mute = exec "amixer sset Master 1+ toggle",
mic_mute = exec "amixer sset Capture 1+ toggle",
printscrn = screenshot ".",
shift + printscrn = capture ".",
super + shift + enter = exec "alacritty",
super + shift + c = close,
super + shift + q = quit,
super + f = toggleFullscreen,
super + l = exec "xsecurelock",
super + 1 = goToWorkspace 1,
super + 2 = goToWorkspace 2,
super + 3 = goToWorkspace 3,
super + 4 = goToWorkspace 4,
super + 5 = goToWorkspace 5,
super + 6 = goToWorkspace 6,
super + 7 = goToWorkspace 7,
super + 8 = goToWorkspace 8,
super + 9 = goToWorkspace 9,
super + shift + 1 = sendToWorkspace 1,
super + shift + 2 = sendToWorkspace 2,
super + shift + 3 = sendToWorkspace 3,
super + shift + 4 = sendToWorkspace 4,
super + shift + 5 = sendToWorkspace 5,
super + shift + 6 = sendToWorkspace 6,
super + shift + 7 = sendToWorkspace 7,
super + shift + 8 = sendToWorkspace 8,
super + shift + 9 = sendToWorkspace 9,
super + up = move 0 -2,
super + down = move 0 +2,
super + left = move -2 0,
super + right = move +2 0,
super + shift + up = resize 0 -2,
super + shift + down = resize 0 +2,
super + shift + left = resize -2 0,
super + shift + right = resize +2 0,
`

// goToWorkspace returns a keybind hook that switches to the workspace at idx.
func goToWorkspace(idx int) func(*wm.WM, *wm.Window) error {
	return func(wm *wm.WM, _ *wm.Window) error {
		wm.Desktop.GoTo(wm, idx)
		return nil
	}
}

// sendToWorkSpace returns a keybind hook that moves the currently focused window to workspace at idx.
func sendToWorkspace(idx int) func(*wm.WM, *wm.Window) error {
	return func(wm *wm.WM, window *wm.Window) error {
		return wm.Desktop.SendToWorkspace(wm, idx)
	}
}

// killWindow returns a keybind hook that toggles fullscreen status of current window.
func killWindow() func(*wm.WM, *wm.Window) error {
	return func(wm *wm.WM, focused *wm.Window) error {
		if focused != nil {
			return focused.Kill(wm)
		}
		return nil
	}
}

// toggleWindowFullscreen returns a keybind hook that toggles fullscreen status of current window.
func toggleWindowFullscreen() func(*wm.WM, *wm.Window) error {
	return func(wm *wm.WM, focused *wm.Window) error {
		if focused != nil {
			return focused.ToggleFullscreen(wm)
		}
		return nil
	}
}

// moveWindow returns a keybind hook that moves the current window by given x,y co-oord changes.
func moveWindow(dx int32, dy int32) func(*wm.WM, *wm.Window) error {
	return func(wm *wm.WM, focused *wm.Window) error {
		if focused != nil {
			return focused.Move(wm, dx, dy)
		}
		return nil
	}
}

// resizeWindow returns a keybind hook that resizes the current window by given x,y co-ord changes.
func resizeWindow(dx int32, dy int32) func(*wm.WM, *wm.Window) error {
	return func(wm *wm.WM, focused *wm.Window) error {
		if focused != nil {
			return focused.Resize(wm, dx, dy)
		}
		return nil
	}
}

// func fitToFree() func(*wm.WM, *wm.Window) error {
// 	return func(wm *wm.WM, focused *wm.Window) error {
// 		if focused != nil {
// 			id := focused.ID
// 			ws := wm.Desktop.Get()
// 			return ws.FitToFree(wm, id)
// 		}
// 		return nil
// 	}
// }

// screenshotRoot returns a keybind hook that will capture the root window, saving to given dir path.
func screenshotRoot(dirpath string) func(*wm.WM, *wm.Window) error {
	return func(wm *wm.WM, focused *wm.Window) error {
		// Calculate timestamped screenshot filepath.
		name := time.Now().Format(ScreenshotFmt)
		path := dirpath + "/" + name

		// Screen root window.
		return screenshot(wm,
			&wm.Desktop.Screen,
			path,
		)
	}
}

// screenshotWindow returns a keybind hook that will capture the focused (else, root) window, saving to given dir path.
func screenshotWindow(dirpath string) func(*wm.WM, *wm.Window) error {
	return func(wm *wm.WM, focused *wm.Window) error {
		// Calculate timestamped screenshot filepath.
		name := time.Now().Format(ScreenshotFmt)
		path := dirpath + "/" + name

		if focused == nil {
			// Screen root window.
			return screenshot(wm,
				&wm.Desktop.Screen,
				path,
			)
		}

		// Screenshot the focused window.
		return screenshot(wm, focused, path)
	}
}

// screenshot will take a capture of given window saving as PNG to file path.
func screenshot(wm *wm.WM, window *wm.Window, path string) error {
	// Take image capture of window.
	img, err := window.Capture(wm)
	if err != nil {
		return err
	}

	// Open new file (truncated) at path.
	file, _, err := fsys.OpenFile(path,
		unix.O_CREAT|unix.O_TRUNC|unix.O_RDWR,
		0o644,
	)
	if err != nil {
		return err
	}

	// Ensure closed.
	defer file.Close()

	// Encode this raw image to PNG on disk.
	if err := png.Encode(file, img); err != nil {
		defer unix.Unlink(path) // delete
		return err
	}

	return nil
}

// firstField returns string up to first instance of space.
func firstField(in string) string {
	if idx := strings.IndexFunc(in, unicode.IsSpace); idx >= 0 {
		return in[:idx]
	}
	return in
}

// mustScanf will scan input string as format, into provided arguments, panicking on error.
func mustScanf(in string, format string, args ...any) {
	if strings.Count(format, "%") == 0 && in != format {
		panic(fmt.Sprintf("%q accepts no args", format))
	}
	if n, err := fmt.Sscanf(in, format, args...); err != nil {
		panic(fmt.Sprintf("error parsing %q as %q: %v", in, format, err))
	} else if n != len(args) {
		panic(fmt.Sprintf("incorrect arg count %d for %q", n, format))
	}
}

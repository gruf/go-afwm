package main

import (
	"strings"

	"codeberg.org/gruf/go-afwm/wm"
	"codeberg.org/gruf/go-xgb/xkeys"
	"codeberg.org/gruf/go-xgb/xproto"
)

// parseKeyBinds parses a slice of keybinds from input strings.
func parseKeyBinds(in []string) []wm.KeyBind {
	keybinds := make([]wm.KeyBind, len(in))
	for i, in := range in {
		keybinds[i] = parseKeyBind(in)
	}
	return keybinds
}

// parseKeyBind will parse a keybind from input string.
func parseKeyBind(in string) wm.KeyBind {
	// Find index of separating '='.
	idx := strings.Index(in, "=")
	if idx == -1 {
		panic("invalid keybind: " + in)
	}

	// Drop ALL spaces and make key part lowercase.
	kpm := strings.ReplaceAll(in[:idx], " ", "")
	kpm = strings.ToLower(kpm)

	// Trim all pre/suf space from bind.
	bind := strings.TrimSpace(in[idx+1:])

	// Parse the actual key + modifiers.
	mods, key := parseKeyPlusMods(kpm)

	// Parse function from bind.
	fn := parseFunc(bind)

	return wm.KeyBind{
		Mods: mods,
		Key:  key,
		Func: fn,
	}
}

// parseKeyPlusMods will parse key symbol with modifier key mask from input string.
func parseKeyPlusMods(in string) (uint16, xproto.Keysym) {
	idx := strings.LastIndex(in, "+")
	if idx == -1 {
		return 0, parseKey(in)
	}
	return parseMods(in[:idx]),
		parseKey(in[idx+1:])
}

// parseKey will parse key symbol from input string.
func parseKey(in string) xproto.Keysym {
	if len(in) == 1 {
		switch c := in[0]; {
		// a-z,0-9 values are direct
		// translations to key symbols
		case '0' <= c && c <= '9':
			return xproto.Keysym(c)
		case 'a' <= c && c <= 'z':
			return xproto.Keysym(c)
		}
	} else {
		// Words -> keys.
		switch in {
		case "up":
			return xkeys.XK_Up
		case "down":
			return xkeys.XK_Down
		case "left":
			return xkeys.XK_Left
		case "right":
			return xkeys.XK_Right
		case "enter":
			return xkeys.XK_Return
		case "space":
			return xkeys.XK_space
		case "backspace":
			return xkeys.XK_BackSpace
		case "printscrn":
			return xkeys.XK_Print
		case "audio_up":
			return xkeys.XF86XK_AudioRaiseVolume
		case "audio_down":
			return xkeys.XF86XK_AudioLowerVolume
		case "audio_mute":
			return xkeys.XF86XK_AudioMute
		case "mic_mute":
			return xkeys.XF86XK_AudioMicMute
		case "bright_up":
			return xkeys.XF86XK_MonBrightnessUp
		case "bright_down":
			return xkeys.XF86XK_MonBrightnessDown
		}
	}
	panic("unrecognized key: " + in)
}

// parseMods will parse modifier key mask from input string.
func parseMods(in string) uint16 {
	var mods uint16
	for _, mod := range strings.Split(in, "+") {
		switch mod {
		case "ctrl", "ctl":
			mods |= xproto.ModMaskControl
		case "alt":
			mods |= xproto.ModMask1
		case "shift":
			mods |= xproto.ModMaskShift
		case "super":
			mods |= xproto.ModMask4
		default:
			panic("unrecognized modifer: " + mod)
		}
	}
	return mods
}

// parseFunc will parse keybind function from input string.
func parseFunc(in string) func(*wm.WM, *wm.Window) error {
	switch firstField(in) {
	case "quit":
		mustScanf(in, "quit")
		return func(wm *wm.WM, _ *wm.Window) error {
			wm.Stop()
			return nil
		}
	case "goToWorkspace":
		var index int
		mustScanf(in, "goToWorkspace %d", &index)
		return goToWorkspace(index - 1)
	case "sendToWorkspace":
		var index int
		mustScanf(in, "sendToWorkspace %d", &index)
		return sendToWorkspace(index - 1)
	case "close":
		mustScanf(in, "close")
		return killWindow()
	case "toggleFullscreen":
		mustScanf(in, "toggleFullscreen")
		return toggleWindowFullscreen()
	case "move":
		var dx, dy int32
		mustScanf(in, "move %d %d", &dx, &dy)
		return moveWindow(dx, dy)
	case "resize":
		var dx, dy int32
		mustScanf(in, "resize %d %d", &dx, &dy)
		return resizeWindow(dx, dy)
	// case "fit":
	// 	mustScanf(in, "fit")
	// 	return fitToFree()
	case "capture":
		var dir string
		mustScanf(in, "capture %q", &dir)
		return screenshotWindow(dir)
	case "screenshot":
		var dir string
		mustScanf(in, "screenshot %q", &dir)
		return screenshotRoot(dir)
	case "exec":
		var cmd string
		mustScanf(in, "exec %q", &cmd)
		return shexec(cmd)
	default:
		panic("unrecognized func: " + in)
	}
}

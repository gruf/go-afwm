package wm

import (
	"codeberg.org/gruf/go-xgb"
	"codeberg.org/gruf/go-xgb/xproto"
)

// Event wraps an XEvent to provide easy access to Window ID.
type Event struct {
	// Window is the window acted on by event.
	Window xproto.Window

	// Event is the actual wrapped X event.
	Event xgb.XEvent

	// linked evs.
	next *Event
	prev *Event
}

// EventQueue is a doubly-linked list of Event objects.
type EventQueue struct {
	head *Event
	tail *Event
}

// PopBy pops the first Event with given ID from queue, or returns nil.
func (eq *EventQueue) PopBy(id xproto.Window) *Event {
	for next := eq.head; next != eq.tail; next = next.next {
		if next.Window == id {
			eq.unlink(next)
			return next
		}
	}
	return nil
}

// Push pushes the given event to the back of the queue.
func (l *EventQueue) Push(elem *Event) {
	if l.head == nil {
		// Set new tail + head
		l.head = elem
		l.tail = elem

		// Link elem to itself
		elem.next = elem
		elem.prev = elem
	} else {
		oldTail := l.tail

		// Link up to head
		elem.next = l.head
		l.head.prev = elem

		// Link to old tail
		elem.prev = oldTail
		oldTail.next = elem

		// Set new tail
		l.tail = elem
	}
}

func (eq *EventQueue) unlink(elem *Event) {
	if eq.head == eq.tail {
		// Drop elem's links
		elem.next = nil
		elem.prev = nil

		// Only elem in list
		eq.head = nil
		eq.tail = nil
		return
	}

	// Get surrounding elems
	next := elem.next
	prev := elem.prev

	// Relink chain
	next.prev = prev
	prev.next = next

	switch elem {
	// Set new head
	case eq.head:
		eq.head = next

	// Set new tail
	case eq.tail:
		eq.tail = prev
	}

	// Drop elem's links
	elem.next = nil
	elem.prev = nil
}

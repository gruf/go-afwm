package wm

type Config struct {
	// MinWindowWidth is the minimum window width (px) to support.
	MinWindowWidth uint32

	// MinWindowHeight is the minimum window height (px) to support.
	MinWindowHeight uint32

	// ScreenPadding is the minimum px of windows to allow on-screen,
	// and the default minimimum px from edges of newly spawned windows.
	ScreenPadding uint32

	// ModKey @TODO
	ModKey uint16

	// KeyBinds @TODO
	KeyBinds []KeyBind
}

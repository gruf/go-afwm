package wm

import "codeberg.org/gruf/go-xgb/xproto"

type KeyBind struct {
	Mods uint16

	Key xproto.Keysym

	Func func(wm *WM, selected *Window) error
}

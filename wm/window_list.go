package wm

import (
	"codeberg.org/gruf/go-xgb/xproto"
)

// Windows represents a list of windows.
type WindowList struct {
	W []Window
}

// Front fetches the Window at the front of the list.
func (w *WindowList) Front() *Window {
	if len(w.W) == 0 {
		return nil
	}
	return &w.W[0]
}

// Back fetches the Window at the back of the list.
func (w *WindowList) Back() *Window {
	if len(w.W) == 0 {
		return nil
	}
	return &w.W[len(w.W)-1]
}

// MoveFront attempts to move window with ID to front of stack, returning
// bool success state and ID of the previous front window if move occurred.
func (w *WindowList) MoveFront(id xproto.Window) (bool, xproto.Window) {
	idx := -1

	// Look for window to focus
	for i := range w.W {
		if w.W[i].ID == id {
			idx = i
			break
		}
	}

	switch {
	// window ID not found
	case idx < 0:
		return false, 0

	// window already front
	case idx == 0:
		return true, 0

	default:
		// window newly moved
		focused := w.W[idx]

		// Shift windows ahead of idx down 1
		_ = copy(w.W[1:idx+1], w.W[:idx])

		// Move new to front
		w.W[0] = focused

		// Return previous front
		return true, w.W[1].ID
	}
}

// MoveFront attempts to move window with ID to front of stack, returning
// bool success state and ID of the previous front window if move occurred.
func (w *WindowList) MoveBack(id xproto.Window) (bool, xproto.Window) {
	idx := -1

	// Look for window to focus
	for i := range w.W {
		if w.W[i].ID == id {
			idx = i
			break
		}
	}

	switch {
	// window ID not found
	case idx < 0:
		return false, 0

	// window already back
	case idx == len(w.W)-1:
		return true, 0

	default:
		// window newly moved
		focused := w.W[idx]

		// Shift windows ahead of idx down 1
		_ = copy(w.W[1:idx+1], w.W[:idx])

		// Move new to front
		w.W[0] = focused

		// Return previous front
		return true, w.W[1].ID
	}
}

// Get will fetch the Window with ID.
func (w *WindowList) Get(id xproto.Window) *Window {
	for i := range w.W {
		if w.W[i].ID == id {
			return &w.W[i]
		}
	}
	return nil
}

// Has checks if Windows contains a Window with ID.
func (w *WindowList) Has(id xproto.Window) bool {
	for i := range w.W {
		if w.W[i].ID == id {
			return true
		}
	}
	return false
}

// PushFront will add the supplied Window to the front of the list.
func (w *WindowList) PushFront(ww Window) {
	l := len(w.W)

	if !w.reslice() {
		// Create new slice with extra cap
		nw := make([]Window, l+1, cap(w.W)+10)

		// Copy old starting from 1+
		copy(nw[1:], w.W)

		// set new slice
		w.W = nw
	} else {
		// Move slice up one place
		copy(w.W[1:], w.W[:l])
	}

	// Set first elem to 'ww'
	w.W[0] = ww
}

// PushBack will add the supplied Window to the back of the list.
func (w *WindowList) PushBack(ww Window) {
	l := len(w.W)

	if !w.reslice() {
		// Create new slice with extra cap
		nw := make([]Window, l+1, cap(w.W)+10)

		// Copy over old
		copy(nw, w.W)

		// set new slice
		w.W = nw
	}

	// Last elem is 'ww'
	w.W[l] = ww
}

// Pop will attempt to remove Window with ID from the list.
func (w *WindowList) Pop(id xproto.Window) *Window {
	pop := -1

	// Look for window with ID
	for i := range w.W {
		if w.W[i].ID == id {
			pop = i
			break
		}
	}

	// Check if found
	if pop < 0 {
		return nil
	}

	// Get window to be popped
	window := w.W[pop]

	// Reslice around the old element
	w.W = append(w.W[:pop], w.W[pop+1:]...)

	return &window
}

// PopFront will remove the Window from the front of the list.
func (w *WindowList) PopFront() *Window {
	// Ensure we have windows
	if len(w.W) == 0 {
		return nil
	}

	// Get front, move slice down 1, reslice
	front := w.W[0]
	copy(w.W, w.W[1:])
	w.W = w.W[:len(w.W)-1]

	return &front
}

// PopBack will remove the Window from the back of the list.
func (w *WindowList) PopBack() *Window {
	// Ensure we have windows
	if len(w.W) == 0 {
		return nil
	}

	// Get back, reslice without last
	back := w.W[len(w.W)-1]
	w.W = w.W[:len(w.W)-1]

	return &back
}

// reslice attempts to reslice with extra slot in slice.
func (w *WindowList) reslice() bool {
	var ok bool
	if ok = len(w.W) < cap(w.W); ok {
		w.W = w.W[:len(w.W)+1]
	}
	return ok
}

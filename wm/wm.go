package wm

import (
	"net"

	"codeberg.org/gruf/go-logger/v4/log"
	"codeberg.org/gruf/go-xgb"
	"codeberg.org/gruf/go-xgb/pkg/xcursor"
	"codeberg.org/gruf/go-xgb/pkg/xprop"
	"codeberg.org/gruf/go-xgb/xproto"
)

const (
	// MouseModeGround indicates unset mousemode (i.e. not selected).
	MouseModeGround = uint8(0)

	// MouseModeMove indicates mouse currently being used to move window.
	MouseModeMove = uint8(1)

	// MouseModeResize indicates mouse currently being used to resize window.
	MouseModeResize = uint8(2)
)

type WM struct {
	// Configuration data.
	Config Config

	// Desktop manages the list of workspaces.
	Desktop Desktop

	// Interned X atoms.
	Atoms InternedAtoms

	// Last mouse pos + mode.
	LastMouseX int32
	LastMouseY int32
	MouseMode  uint8

	// Currrently selected X window.
	Selected xproto.Window

	// Map of keycodes to keysyms.
	KBMap KeyboardMap

	// XSetup contains X connection setup information.
	XSetup *xproto.SetupInfo

	// EvQueue stores queued events for windows not yet mapped
	EvQueue EventQueue

	// XConn is the underlying X connection.
	XConn *xgb.XConn

	// XProp provides xprop conn helper methods.
	XProp *xprop.XPropConn
}

func (wm *WM) Init(display string) error {
	log.Infof("connecting to x display %s", display)

	// Attempt to connect to X
	conn, info, err := xgb.Dial(display)
	if err != nil {
		return wrapErr("error dialing x connection", err)
	}

	// Set and prepare
	// X connnection and
	// X property manager.
	wm.XConn = conn
	wm.XProp = new(xprop.XPropConn)
	wm.XProp.XConn = conn

	// Setup xprotocol with our X connection
	setup, err := xproto.Setup(wm.XConn, info)
	if err != nil {
		return wrapErr("error decoding setup info", err)
	}

	// Add setup info
	wm.XSetup = setup

	// Update root window information
	root := setup.Roots[0].Root
	wm.Desktop.Screen.ID = root

	// Intern required atoms
	if err := wm.Atoms.InternAll(wm); err != nil {
		return wrapErr("error interning atoms", err)
	}

	// Register root window for events
	if err := xproto.ChangeWindowAttributes(
		wm.XConn,
		root,
		xproto.CwEventMask,
		[]uint32{xproto.EventMaskSubstructureRedirect | xproto.EventMaskStructureNotify},
	); err != nil {
		return wrapErr("error setting root event mask", err)
	}

	// Set supported atoms
	if err := wm.XProp.ChangePropName(
		root,
		32, // 32bit data
		"_NET_SUPPORTED",
		"ATOM",
		xprop.FromData32([]uint32{
			uint32(wm.Atoms.WMProtocols.Atom),
			uint32(wm.Atoms.WMDeleteWindow.Atom),
		}),
	); err != nil {
		return wrapErr("error setting supported protocols", err)
	}

	// Get current X keyboard mapping
	kbmap, err := xproto.GetKeyboardMapping(
		wm.XConn,
		setup.MinKeycode,
		byte(setup.MaxKeycode-setup.MinKeycode+1),
	)
	if err != nil {
		return wrapErr("error getting keyboard mapping", err)
	}

	// Update our internal stored keyboard map
	wm.KBMap.Update(setup.MinKeycode, kbmap)

	// For configured keybinds, register grab on root
	for i := range wm.Config.KeyBinds {
		kb := &wm.Config.KeyBinds[i]
		for _, kc := range wm.KBMap.GetKeycode(kb.Key) {
			if err := xproto.GrabKey(
				wm.XConn,
				false,
				root,
				kb.Mods,
				kc,
				xproto.GrabModeAsync,
				xproto.GrabModeAsync,
			); err != nil {
				return wrapErr("error grabbing key", err)
			}
		}
	}

	// Grab left click on root window
	if err := xproto.GrabButton(
		wm.XConn,
		false,
		root,
		xproto.EventMaskButtonPress|xproto.EventMaskButtonRelease,
		xproto.GrabModeAsync,
		xproto.GrabModeAsync,
		root,
		xproto.CursorNone,
		xproto.ButtonIndex1,
		wm.Config.ModKey,
	); err != nil {
		return wrapErr("error grabbing left click", err)
	}

	// Grab right click on root window
	if err := xproto.GrabButton(
		wm.XConn,
		false,
		root,
		xproto.EventMaskButtonPress|xproto.EventMaskButtonRelease,
		xproto.GrabModeAsync,
		xproto.GrabModeAsync,
		root,
		xproto.CursorNone,
		xproto.ButtonIndex3,
		wm.Config.ModKey,
	); err != nil {
		return wrapErr("error grabbing right click", err)
	}

	// Create necessary cursor for root window
	curs, err := xcursor.CreateCursor(wm.XConn, xcursor.LeftPtr)
	if err != nil {
		return wrapErr("error creating cursor", err)
	}

	// Set our root window cursor attribute
	if err := xproto.ChangeWindowAttributes(
		wm.XConn,
		root,
		xproto.CwCursor,
		[]uint32{uint32(curs)},
	); err != nil {
		return wrapErr("error setting root cursor", err)
	}

	// Fetch initial screen geometry for the root window
	geom, err := xproto.GetGeometry(wm.XConn, xproto.Drawable(root))
	if err != nil {
		return wrapErr("error getting root geometry", err)
	}

	// Set desktop screen values
	wm.Desktop.Screen.X = int32(geom.X)
	wm.Desktop.Screen.Y = int32(geom.Y)
	wm.Desktop.Screen.Width = uint32(geom.Width)
	wm.Desktop.Screen.Height = uint32(geom.Height)

	// Query tree for existing windows in X
	tree, err := xproto.QueryTree(wm.XConn, root)
	if err != nil {
		return wrapErr("error querying root tree", err)
	}

	for _, child := range tree.Children {
		// Get attributes for given window (on error, assume closed)
		attr, err := xproto.GetWindowAttributes(wm.XConn, child)
		if err != nil {
			log.Errorf("error getting child attributes: %v", err)
			continue
		}

		// Ignore invisible / override redirect windows
		if attr.MapState != xproto.MapStateViewable ||
			attr.OverrideRedirect {
			continue
		}

		// Handle this as a map request
		onMapRequest(wm, &xproto.MapRequestEvent{
			Sequence: attr.Sequence,
			Parent:   root,
			Window:   child,
		})
	}

	return nil
}

func (wm *WM) Run() error {
	// Log start and stop
	log.Info("started")
	defer log.Info("stopped")

	// Perform initial activation of current workspace
	if err := wm.Desktop.Get().Activate(wm); err != nil {
		return err
	}

	for {
		// Wait on next event from X
		ev, err := wm.XConn.Recv()
		if err != nil {
			if xerr, ok := err.(xgb.XError); ok { //nolint
				log.Errorf("received x error: %v", xerr)
			} else if err == net.ErrClosed { //nolint
				return nil // conn closed
			} else {
				log.Errorf("error receiving event: %v", err)
			}
		}

		// Pass to handler.
		wm.handle(ev)
	}
}

func (wm *WM) Stop() {
	for i := 0; i < 9; i++ {
		// Close all windows in each workspace
		if err := wm.Desktop.GetIdx(i).Iterate(func(w *Window) error {
			return w.Kill(wm)
		}); err != nil {
			log.Errorf("error killing windows: %v", err)
		}
	}

	// Close X conn.
	_ = wm.XConn.Close()
}

func (wm *WM) handle(ev xgb.XEvent) {
	log.Debugf("%[1]T%+[1]v", ev)
	switch ev := ev.(type) {
	case *xproto.ConfigureNotifyEvent:
		onConfigureNotify(wm, ev)
	case *xproto.ConfigureRequestEvent:
		onConfigureRequest(wm, ev)
	case *xproto.CirculateRequestEvent:
		onCirculateRequest(wm, ev)
	case *xproto.MapRequestEvent:
		onMapRequest(wm, ev)
	case *xproto.UnmapNotifyEvent:
		onUnmapNotify(wm, ev)
	case *xproto.DestroyNotifyEvent:
		onDestroyNotify(wm, ev)
	case *xproto.EnterNotifyEvent:
		onEnterNotify(wm, ev)
	case *xproto.MotionNotifyEvent:
		onMotionNotify(wm, ev)
	case *xproto.ButtonPressEvent:
		onButtonPress(wm, ev)
	case *xproto.ButtonReleaseEvent:
		onButtonRelease(wm)
	case *xproto.KeyPressEvent:
		onKeyPress(wm, ev)
	// ignored event types
	// case *xproto.KeyReleaseEvent:
	// case *xproto.CirculateNotifyEvent:
	// case *xproto.GravityNotifyEvent:
	// case *xproto.ReparentNotifyEvent:
	// case *xproto.ClientMessageEvent:
	default:
		log.Debug("unhandled event type")
	}
}

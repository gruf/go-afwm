package wm

// wrappedError wraps an error with msg string prefix.
type wrappedError struct {
	msg string
	err error
}

func (err *wrappedError) Error() string {
	return err.msg + ": " + err.err.Error()
}

// wrapErr wraps error to be prefixed by given string.
func wrapErr(msg string, err error) error {
	return &wrappedError{msg: msg, err: err}
}

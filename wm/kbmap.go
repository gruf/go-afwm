package wm

import (
	"codeberg.org/gruf/go-xgb/xproto"
)

// KeyboardMap stores a map of X keycodes to possible keysyms.
type KeyboardMap struct {
	keysymMap [256]([]xproto.Keysym)
}

// GetKeysym fetches the keysym mapped to provided keycode.
func (km *KeyboardMap) GetKeysym(kc xproto.Keycode) xproto.Keysym {
	ks := km.keysymMap[kc]
	if len(ks) == 0 {
		return 0
	}
	return ks[0]
}

// GetKeycode fetches a mapped list of possible keycodes for given keysym.
func (km *KeyboardMap) GetKeycode(ks xproto.Keysym) []xproto.Keycode {
	var kcs []xproto.Keycode
	for kc := range km.keysymMap {
		kss := km.keysymMap[kc]
		kc := xproto.Keycode(kc)
		for j := range kss {
			if kss[j] == ks {
				kcs = append(kcs, kc)
			}
		}
	}
	return kcs
}

// Update will update the keyboard mappings stored with the values in provided reply.
func (km *KeyboardMap) Update(kmin xproto.Keycode, kbmap xproto.GetKeyboardMappingReply) {
	// Determine total no. keycodes
	keycodes := kbmap.Length / uint32(kbmap.KeysymsPerKeycode)

	// Zero out the map
	for i := range km.keysymMap {
		km.keysymMap[i] = km.keysymMap[i][:0]
	}

	// Generate new mappings
	for kci := uint32(0); kci < keycodes; kci++ {
		// Determine actual keycode
		kc := kmin + xproto.Keycode(kci)

		for ksi := uint32(0); ksi < uint32(kbmap.KeysymsPerKeycode); ksi++ {
			// Determine actual keysym for keycode
			ks := kbmap.Keysyms[ksi+kci*uint32(kbmap.KeysymsPerKeycode)]

			// Check not mapped already
			for i := range km.keysymMap[kc] {
				if km.keysymMap[kc][i] == ks {
					continue
				}
			}

			// Add the keycode mapping
			km.keysymMap[kc] = append(km.keysymMap[kc], ks)
		}
	}
}

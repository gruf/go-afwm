package wm

import (
	"image"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-xgb/pkg/xprop"
	"codeberg.org/gruf/go-xgb/xproto"
)

type Window struct {
	// X window ID.
	ID xproto.Window

	// Represented by a rectangle.
	Rectangle

	// Tracks Window fullscreen status.
	Fullscreen bool

	// Protocols is the list of protocols this window supports.
	Protocols []xproto.Atom
}

// CreateWindow attemps to create and populate a Window object for given X window ID.
func CreateWindow(wm *WM, id xproto.Window) (Window, error) {
	w := Window{ID: id}

	// Fetch window supported protocol information.
	if err := w.GetSupportedProtocols(wm); err != nil {
		return w, err
	}

	// Fetch window initial geometry information.
	if err := w.GetGeometry(wm); err != nil {
		return w, err
	}

	return w, nil
}

// GetSupportedProtocols fetches the supported WM protocols for this w.
func (w *Window) GetSupportedProtocols(wm *WM) error {
	// Query X for window _NET_WM_PROTOCOLS values.
	reply, err := wm.XProp.GetProp(w.ID, wm.Atoms.WMProtocols.Atom)
	if err != nil {
		return wrapErr("error getting supported protocols", err)
	}

	// Extract list of atom IDs from X reply.
	protos, err := xprop.PropValAtoms(wm.XProp, reply)
	if err != nil {
		return wrapErr("error decoding supported protocol atoms", err)
	}

	// Update protocols
	w.Protocols = protos

	return nil
}

// SupportsProtocol returns whether this window supports given X protocol.
func (w *Window) SupportsProtocol(proto xproto.Atom) bool {
	for _, p := range w.Protocols {
		if p == proto {
			return true
		}
	}
	return false
}

// GetGeometry fetches updated geometry from X.
func (w *Window) GetGeometry(wm *WM) error {
	// Fetch current window geometry from X.
	reply, err := xproto.GetGeometry(wm.XConn, xproto.Drawable(w.ID))
	if err != nil {
		return wrapErr("error getting geometry", err)
	}

	// Set window geoemtry.
	w.X = int32(reply.X)
	w.Y = int32(reply.Y)
	w.Width = uint32(reply.Width)
	w.Height = uint32(reply.Height)

	return nil
}

// ToggleFullscreen toggles the window's fullscreen status.
func (w *Window) ToggleFullscreen(wm *WM) error {
	if !w.Fullscreen {
		// not fullscreen, set it!
		return w.SetFullscreen(wm)
	} else {
		// reconfigure existing geometry.
		return w.SetGeometry(wm, w.Rectangle)
	}
}

// UpdateGeometry will call SetGeometry only on the condition that window is NOT
// currently fullscreen, and that updated geometry is actually changed from existing.
func (w *Window) UpdateGeometry(wm *WM, rect Rectangle) error {
	if w.Fullscreen || w.Rectangle == rect {
		// Either fullscreen, or there are no
		// geometry changes. Nothnig to do!
		return nil
	}
	return w.SetGeometry(wm, rect)
}

// SetGeometry will bound check the given geometry against current configuration and root
// window dimensions, then send a configure window request to X with updatee dimensions.
func (w *Window) SetGeometry(wm *WM, rect Rectangle) error {
	if rect.Width < wm.Config.MinWindowWidth {
		// Ensure a non-zero width window.
		rect.Width = wm.Config.MinWindowWidth
	}

	if rect.Height < wm.Config.MinWindowHeight {
		// Ensure a non-zero height window.
		rect.Height = wm.Config.MinWindowHeight
	}

	// Calculate (X,Y) coordinate minimum and maximum boundary positions.
	x_min := wm.Desktop.Screen.X + int32(wm.Config.ScreenPadding) - int32(rect.Width)
	x_max := wm.Desktop.Screen.X_Max() - int32(wm.Config.ScreenPadding)
	y_min := wm.Desktop.Screen.Y + int32(wm.Config.ScreenPadding) - int32(rect.Height)
	y_max := wm.Desktop.Screen.Y_Max() - int32(wm.Config.ScreenPadding)

	// Check X bounds.
	if rect.X < x_min {
		rect.X = x_min
	} else if w.X > x_max {
		rect.X = x_max
	}

	// Check Y bounds.
	if w.Y < y_min {
		rect.Y = y_min
	} else if w.Y > y_max {
		rect.Y = y_max
	}

	// Send the configure window geometry request to X.
	if err := xproto.ConfigureWindowUnchecked(wm.XConn, w.ID,
		// Always set the full range of window dimensions
		// on each call. It accounts for weird sync issues
		// with window geometry betwen X and afwm.
		xproto.ConfigWindowX|xproto.ConfigWindowY|
			xproto.ConfigWindowWidth|xproto.ConfigWindowHeight,
		[]uint32{uint32(rect.X), uint32(rect.Y), rect.Width, rect.Height},
	); err != nil {
		return wrapErr("error configuring window geometry", err)
	}

	// Set updated geometry.
	w.Rectangle = rect

	// Mark as NOT fullscreen.
	w.Fullscreen = false

	return nil
}

// SetFullscreen sets the window as fullscreen updating the relevant flag,
// and sending a configure window request to X with current root window dimensions.
func (w *Window) SetFullscreen(wm *WM) error {
	// Send fullscreen window configuration to X
	if err := xproto.ConfigureWindowUnchecked(
		wm.XConn, w.ID,
		xproto.ConfigWindowX|xproto.ConfigWindowY|
			xproto.ConfigWindowWidth|xproto.ConfigWindowHeight,
		[]uint32{
			uint32(wm.Desktop.Screen.X),
			uint32(wm.Desktop.Screen.Y),
			uint32(wm.Desktop.Screen.Width),
			uint32(wm.Desktop.Screen.Height),
		},
	); err != nil {
		return wrapErr("error setting fullscreen window", err)
	}

	// Mark as fullscreen.
	w.Fullscreen = true

	return nil
}

// Move will move this window's x,y coords by given dx,dy.
func (w *Window) Move(wm *WM, dx int32, dy int32) error {
	if w.Fullscreen {
		return nil
	}
	rect := w.Rectangle
	rect.X += dx
	rect.Y += dy
	return w.SetGeometry(wm, rect)
}

// Resize will resize this window's width,height by given dx,dy.
func (w *Window) Resize(wm *WM, dx int32, dy int32) error {
	if w.Fullscreen {
		return nil
	}
	rect := w.Rectangle
	rect.Width += uint32(dx)
	rect.Height += uint32(dy)
	return w.SetGeometry(wm, rect)
}

// Kill will close this window (either via client message, or destroy).
func (w *Window) Kill(wm *WM) error {
	if w.SupportsProtocol(wm.Atoms.WMDeleteWindow.Atom) {
		// This window supports ICCCM method of WM_DELETE_WINDOW
		msgData := xproto.ClientMessageDataUnionData32New([]uint32{
			uint32(wm.Atoms.WMDeleteWindow.Atom),
			uint32(xproto.TimeCurrentTime), 0, 0, 0,
		})

		// Prepare client message event
		ev := xproto.ClientMessageEvent{
			Format: 32,
			Window: w.ID,
			Type:   wm.Atoms.WMProtocols.Atom,
			Data:   msgData,
		}

		// Send the event to X
		if err := xproto.SendEventUnchecked(
			wm.XConn,
			false,
			w.ID,
			xproto.EventMaskNoEvent,
			byteutil.B2S(ev.Bytes()),
		); err != nil {
			return wrapErr("error sending event", err)
		}
	} else {
		// Use plain-old X.DestroyWindow
		if err := xproto.DestroyWindowUnchecked(
			wm.XConn,
			w.ID,
		); err != nil {
			return wrapErr("error destroying window", err)
		}
	}

	return nil
}

// Capture will return an RGBA image object representing the current image of this window on-display.
func (w *Window) Capture(wm *WM) (*image.RGBA, error) {
	// Attempt to open window ID as image from X.
	raw, err := xproto.GetImage(wm.XConn,
		xproto.ImageFormatZPixmap,
		xproto.Drawable(wm.Desktop.Screen.ID),
		int16(w.X), int16(w.Y),
		uint16(w.Width), uint16(w.Height),
		0xffffffff,
	)
	if err != nil {
		return nil, wrapErr("error getting window image", err)
	}

	for i := 0; i < len(raw.Data); i += 4 {
		// Convert data to usable as RGBA formatted image data.
		raw.Data[i], raw.Data[i+2] = raw.Data[i+2], raw.Data[i]
		raw.Data[i+3] = 255
	}

	// Create rect to
	// represent dimens.
	rect := image.Rect(
		0, // x0
		0, // y0
		int(w.Width),
		int(w.Height),
	)

	// Return RGBA image obj.
	return &image.RGBA{
		Pix:    raw.Data,
		Stride: int(4 * w.Width),
		Rect:   rect,
	}, nil
}

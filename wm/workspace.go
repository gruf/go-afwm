package wm

import (
	"errors"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-kv/format"
	"codeberg.org/gruf/go-logger/v4/log"
	"codeberg.org/gruf/go-xgb/pkg/xprop"
	"codeberg.org/gruf/go-xgb/xproto"
)

// Workspace represents a single workspace of windows.
type Workspace struct {
	Windows WindowList // underlying window storage
}

func (ws *Workspace) Activate(wm *WM) error {
	if len(ws.Windows.W) == 0 {
		return nil // no windows
	}

	if err := ws.IterateRev(func(window *Window) error {
		// Disable events before mapping window
		if err := xproto.ChangeWindowAttributesUnchecked(wm.XConn,
			window.ID,
			xproto.CwEventMask,
			[]uint32{xproto.EventMaskNoEvent},
		); err != nil {
			return wrapErr("error disabling event tracking", err)
		}

		// Map window to the display
		if err := xproto.MapWindowUnchecked(wm.XConn, window.ID); err != nil {
			return wrapErr("error mapping window", err)
		}

		// Enable events again for the window
		if err := xproto.ChangeWindowAttributesUnchecked(wm.XConn,
			window.ID,
			xproto.CwEventMask,
			[]uint32{xproto.EventMaskEnterWindow | xproto.EventMaskStructureNotify},
		); err != nil {
			return wrapErr("error enabling event tracking", err)
		}

		return nil
	}); err != nil {
		return err
	}

	var focusID xproto.Window

	// If pointer on root / within bounds of new, focus it
	reply, err := xproto.QueryPointer(wm.XConn, wm.Desktop.Screen.ID)
	switch {
	case err != nil:
		log.Errorf("Workspace.Activate: error querying pointer: %v", err)
		focusID = ws.Windows.Front().ID
	case reply.Child == 0:
		focusID = ws.Windows.Front().ID
	default:
		focusID = reply.Child
	}

	// focus determined window ID.
	if err := xproto.SetInputFocusUnchecked(wm.XConn,
		xproto.InputFocusPointerRoot,
		focusID,
		xproto.TimeCurrentTime,
	); err != nil {
		return wrapErr("error setting input focus", err)
	}

	return nil
}

func (ws *Workspace) Deactivate(wm *WM) error {
	return ws.Iterate(func(window *Window) error {
		// Disable events before unmapping the window.
		if err := xproto.ChangeWindowAttributesUnchecked(wm.XConn,
			window.ID,
			xproto.CwEventMask,
			[]uint32{xproto.EventMaskNoEvent},
		); err != nil {
			return wrapErr("error disabling event tracking", err)
		}

		// Unmap the window from display.
		if err := xproto.UnmapWindowUnchecked(
			wm.XConn,
			window.ID,
		); err != nil {
			return wrapErr("error unmapping window", err)
		}

		// Enable events again for this window.
		if err := xproto.ChangeWindowAttributesUnchecked(wm.XConn,
			window.ID,
			xproto.CwEventMask,
			[]uint32{xproto.EventMaskEnterWindow | xproto.EventMaskStructureNotify},
		); err != nil {
			return wrapErr("error enabling event tracking", err)
		}

		return nil
	})
}

func (ws *Workspace) AddNew(wm *WM, id xproto.Window) (*Window, error) {
	var types []xproto.Atom

	// Get list of atom IDs for the _NET_WM_WINDOW_TYPE property.
	prop, err := wm.XProp.GetProp(id, wm.Atoms.WMWindowType.Atom)
	if err == nil {
		types, _ = xprop.PropValAtoms(wm.XProp, prop)
	}

	if len(types) == 0 {
		// Chances are it doesn't support _NET_WM_WINDOW_TYPE,
		// so we set default types to ensure it gets tracked.
		types = []xproto.Atom{wm.Atoms.WMWindowTypeNormal.Atom}
	}

	for _, atom := range types {
		switch atom {
		case
			// wm.Atoms.WMWindowTypeDesktop.Atom,      // main desktop feature
			// wm.Atoms.WMWindowTypeDock.Atom,         // desktop dock / panel (always on-top)
			// wm.Atoms.WMWindowTypeNotification.Atom, // literally a notification
			// wm.Atoms.WMWindowTypeDND.Atom,          // being dragged
			// wm.Atoms.WMWindowTypeCombo.Atom,        // e.g. autocomplete
			// wm.Atoms.WMWindowTypeTooltip.Atom,      // e.g. popup "tooltip" text
			wm.Atoms.WMWindowTypeNormal.Atom,
			wm.Atoms.WMWindowTypeDialog.Atom,
			wm.Atoms.WMWindowTypeToolbar.Atom,
			wm.Atoms.WMWindowTypeMenu.Atom,
			wm.Atoms.WMWindowTypePopupMenu.Atom,
			wm.Atoms.WMWindowTypeDropdownMenu.Atom,
			wm.Atoms.WMWindowTypeUtility.Atom,
			wm.Atoms.WMWindowTypeSplash.Atom:

			// Attempt to create new window
			window, err := CreateWindow(wm, id)
			if err != nil {
				return nil, wrapErr("error creating window", err)
			}

			// Add window to current workspace
			if err := wm.Desktop.Get().Add(wm, window); err != nil {
				return nil, wrapErr("error adding window", err)
			}

			return &window, nil
		}
	}

	return nil, nil
}

func (ws *Workspace) Add(wm *WM, window Window) error {
	// If there is currently focused, stack above.
	if focused := ws.Windows.Front(); focused != nil {
		if err := xproto.ConfigureWindowUnchecked(wm.XConn, window.ID,
			xproto.ConfigWindowSibling|xproto.ConfigWindowStackMode,
			[]uint32{uint32(focused.ID), xproto.StackModeAbove},
		); err != nil {
			log.Errorf("error stacking above: %v", err)
		}
	}

	// Internally add window.
	ws.Windows.PushFront(window)

	// Enable events again for this window.
	if err := xproto.ChangeWindowAttributesUnchecked(wm.XConn,
		window.ID,
		xproto.CwEventMask,
		[]uint32{xproto.EventMaskEnterWindow | xproto.EventMaskStructureNotify},
	); err != nil {
		return wrapErr("error enabling event tracking", err)
	}

	return nil
}

func (ws *Workspace) Delete(wm *WM, id xproto.Window) (bool, error) {
	// Pop window from workspace
	window := ws.Windows.Pop(id)
	if window == nil {
		return false, nil
	}

	// Disable window event tracking
	if err := xproto.ChangeWindowAttributesUnchecked(wm.XConn,
		id, xproto.CwEventMask,
		[]uint32{xproto.EventMaskNoEvent},
	); err != nil {
		return true, wrapErr("error disabling event tracking", err)
	}

	return true, nil
}

func (ws *Workspace) MoveTop(wm *WM, id xproto.Window) bool {
	// Attempt to move window to front in list.
	ok, previous := ws.Windows.MoveFront(id)
	if !ok {
		return false
	}

	if previous != 0 {
		// Stack focused above previously focused.
		if err := xproto.ConfigureWindowUnchecked(wm.XConn, id,
			xproto.ConfigWindowSibling|xproto.ConfigWindowStackMode,
			[]uint32{uint32(previous), xproto.StackModeAbove},
		); err != nil {
			log.Errorf("error stacking above: %v", err)
		}
	}

	return true
}

func (ws *Workspace) MoveBottom(wm *WM, id xproto.Window) bool {
	// Attempt to move window to back in list.
	ok, previous := ws.Windows.MoveBack(id)
	if !ok {
		return false
	}

	if previous != 0 {
		// Stack focused above previously focused.
		if err := xproto.ConfigureWindowUnchecked(wm.XConn, id,
			xproto.ConfigWindowSibling|xproto.ConfigWindowStackMode,
			[]uint32{uint32(previous), xproto.StackModeBelow},
		); err != nil {
			log.Errorf("error stacking below: %v", err)
		}
	}

	return true
}

func (ws *Workspace) Focus(wm *WM, id xproto.Window) (bool, error) {
	// Move window to top.
	if !ws.MoveTop(wm, id) {
		return false, nil
	}

	// tell X to focus the window.
	if err := xproto.SetInputFocusUnchecked(wm.XConn,
		xproto.InputFocusPointerRoot,
		id, xproto.TimeCurrentTime,
	); err != nil {
		return true, wrapErr("error setting input focus", err)
	}

	return true, nil
}

func (ws *Workspace) Iterate(fn func(*Window) error) error {
	var buf byteutil.Buffer

	for i := 0; i < len(ws.Windows.W); i++ {
		// Get ptr to window at idx
		win := &ws.Windows.W[i]

		// Perform iter func on this window
		if err := fn(win); err != nil {
			format.Appendf(&buf, "{:k}={:v},", win.ID, err)
		}
	}

	if buf.Len() > 0 {
		// Drop last comma
		buf.Truncate(1)

		// Generate error from accumulated str
		return errors.New(buf.String())
	}

	return nil
}

func (ws *Workspace) IterateRev(fn func(*Window) error) error {
	var buf byteutil.Buffer

	for i := len(ws.Windows.W) - 1; i >= 0; i-- {
		// Get ptr to window at idx
		win := &ws.Windows.W[i]

		// Perform iter func on this window
		if err := fn(win); err != nil {
			format.Appendf(&buf, "{:k}={:v},", win.ID, err)
		}
	}

	if buf.Len() > 0 {
		// Drop last comma
		buf.Truncate(1)

		// Generate error from accumulated str
		return errors.New(buf.String())
	}

	return nil
}

// func (ws *Workspace) FitToFree(wm *WM, id xproto.Window) error {
// 	// Get window at given id.
// 	window := ws.Windows.Get(id)
// 	if window == nil {
// 		return nil
// 	}

// 	// - search + find intersecting rectangle
// 	//   pairs of windows (i.e. where outside
// 	//   lines overlap)
// 	// - find corner points of these rectangle
// 	//   pair intersections
// 	// - corner points to either the other
// 	//   nearest intersection corner point,
// 	//   or the screen edge, is a free space

// 	// Create pixel grid of current screen.
// 	pxs := make([][]bool, wm.Desktop.Screen.Width)
// 	for i := range pxs {
// 		pxs[i] = make([]bool, wm.Desktop.Screen.Height)
// 	}

// 	// Determine window blocking areas.
// 	for _, w := range ws.Windows.W {

// 		// Fullscreen, no space.
// 		if w.Fullscreen {
// 			return nil
// 		}

// 		// Skip resizing window.
// 		if w.ID == window.ID {
// 			continue
// 		}

// 		// Set all pixels in the window area.
// 		for x := w.X; x < w.X_Max(); x++ {
// 			for y := w.Y; y < w.Y_Max(); y++ {
// 				pxs[x][y] = true
// 			}
// 		}
// 	}

// 	if free == nil {
// 		if rect == nil {
// 			// no space.
// 			return nil
// 		}

// 		// use rect.
// 		free = rect
// 	}

// 	log.Infof("window=%d free=%+v", id, free)

// 	if free.Width < wm.Config.MinWindowWidth ||
// 		free.Height < wm.Config.MinWindowHeight {
// 		// no freespace (or basically none)
// 		return nil
// 	}

// 	// Resize window to fit free space.
// 	return window.SetGeometry(wm, *free)
// }

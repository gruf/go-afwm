package wm

import (
	"codeberg.org/gruf/go-logger/v4/log"
	"codeberg.org/gruf/go-xgb/xproto"
)

func onConfigureNotify(wm *WM, ev *xproto.ConfigureNotifyEvent) {
	if ev.Window != wm.Desktop.Screen.ID {
		// the only configure notify we are
		// interested in is the root screen.
		return
	}

	// Set updated root screen geometry.
	wm.Desktop.Screen.X = int32(ev.X)
	wm.Desktop.Screen.Y = int32(ev.Y)
	wm.Desktop.Screen.Width = uint32(ev.Width)
	wm.Desktop.Screen.Height = uint32(ev.Height)

	for i := range wm.Desktop.Workspaces {
		// Get workspace at index.
		ws := wm.Desktop.GetIdx(i)

		// Force reconfigure all windows to bound-check.
		err := ws.Iterate(func(w *Window) error {
			return w.SetGeometry(wm, w.Rectangle)
		})
		if err != nil {
			log.Errorf("error(s) reconfiguring windows: %v", err)
		}
	}
}

func onConfigureRequest(wm *WM, ev *xproto.ConfigureRequestEvent) {
	var window *Window

	for i := range wm.Desktop.Workspaces {
		// Get workspace at index.
		ws := wm.Desktop.GetIdx(i)

		// Try to fetch window from workspace.
		window = ws.Windows.Get(ev.Window)
		if window != nil {
			break
		}
	}

	if window == nil {
		// ignore for now, usually means
		// there's an incoming map-request.
		log.Debugf("queuing event: %+v", ev)
		wm.EvQueue.Push(&Event{
			Window: ev.Window,
			Event:  ev,
		})
		return
	}

	// Build new geometry
	// from existing values.
	rect := window.Rectangle

	// If x mask set, update window.
	if ev.ValueMask&xproto.ConfigWindowX != 0 {
		rect.X = int32(ev.X)
	}

	// If y mask set, update window.
	if ev.ValueMask&xproto.ConfigWindowY != 0 {
		rect.Y = int32(ev.Y)
	}

	// If width mask set, update window.
	if ev.ValueMask&xproto.ConfigWindowWidth != 0 {
		rect.Width = uint32(ev.Width)
	}

	// If height mask set, update window.
	if ev.ValueMask&xproto.ConfigWindowHeight != 0 {
		rect.Height = uint32(ev.Height)
	}

	// Set updated window geometry and send to X.
	if err := window.UpdateGeometry(wm, rect); err != nil {
		log.Error(err)
	}
}

func onCirculateRequest(wm *WM, ev *xproto.CirculateRequestEvent) {
loop:
	for i := range wm.Desktop.Workspaces {
		// Get workspace at index.
		ws := wm.Desktop.GetIdx(i)

		switch ev.Place {
		case xproto.PlaceOnTop:
			// Try to move winddow to top of ws.
			ok := ws.MoveTop(wm, ev.Window)
			if !ok {
				continue loop
			}

		case xproto.PlaceOnBottom:
			// Try to move winddow to bottom of ws.
			ok := ws.MoveBottom(wm, ev.Window)
			if !ok {
				continue loop
			}
		}

		if i == wm.Desktop.Current {
			// This is on current desktop, focus front window.
			if err := xproto.SetInputFocusUnchecked(wm.XConn,
				xproto.InputFocusPointerRoot,
				ws.Windows.Front().ID, xproto.TimeCurrentTime,
			); err != nil {
				log.Errorf("error setting input focus: %v", err)
			}
		}

		return
	}

	// ignore for now, usually means
	// there's an incoming map-request.
	log.Debugf("queuing event: %+v", ev)
	wm.EvQueue.Push(&Event{
		Window: ev.Window,
		Event:  ev,
	})
}

func onMapRequest(wm *WM, ev *xproto.MapRequestEvent) {
	// Attempt to map window to the X display.
	if err := xproto.MapWindowUnchecked(wm.XConn, ev.Window); err != nil {
		log.Errorf("error mapping window: %v", err)
		return
	}

	// If pointer on root / within bounds of new, focus it.
	curs, err := xproto.QueryPointer(wm.XConn, wm.Desktop.Screen.ID)
	if err == nil && (curs.Child == 0 || curs.Child == ev.Window) {
		err = xproto.SetInputFocusUnchecked(wm.XConn,
			xproto.InputFocusPointerRoot,
			ev.Window,
			xproto.TimeCurrentTime,
		)
	}

	if err != nil {
		log.Errorf("error setting input focus: %v", err)
	}

	// Get current workspace.
	cws := wm.Desktop.Get()

	for i := range wm.Desktop.Workspaces {
		// Get workspace at index.
		ws := wm.Desktop.GetIdx(i)

		// Try to pop window from workspace.
		window := ws.Windows.Pop(ev.Window)
		if window == nil {
			continue
		}

		// Push window to front of current workspace.
		if err := cws.Add(wm, *window); err != nil {
			log.Errorf("error adding window: %v", err)
		}

		return
	}

	// Add new window to current workspace.
	window, err := cws.AddNew(wm, ev.Window)
	if err != nil {
		log.Errorf("error adding new window: %v", err)
		return
	} else if window == nil {
		// ignored window type.
		return
	}

	// Force reconfigure current window geometry to bound-check.
	if err := window.SetGeometry(wm, window.Rectangle); err != nil {
		log.Error(err)
	}

	for {
		// Pop all queued events for window.
		queued := wm.EvQueue.PopBy(ev.Window)
		if queued == nil {
			break
		}

		// Handle each popped event from queue.
		log.Debugf("unqueued event: %+v", queued.Event)
		wm.handle(queued.Event)
	}
}

func onUnmapNotify(wm *WM, ev *xproto.UnmapNotifyEvent) {
	for i := range wm.Desktop.Workspaces {
		// Get workspace at index.
		ws := wm.Desktop.GetIdx(i)

		// Try to delete window from workspace.
		if ok, err := ws.Delete(wm, ev.Window); ok {
			if err != nil {
				log.Errorf("error deleting window: %v", err)
			}
			return
		}
	}

	// Drop all queued events for this window.
	for wm.EvQueue.PopBy(ev.Window) != nil {
	}
}

func onDestroyNotify(wm *WM, ev *xproto.DestroyNotifyEvent) {
	for i := range wm.Desktop.Workspaces {
		// Get workspace at index.
		ws := wm.Desktop.GetIdx(i)

		// Try pop window with ID from workspace (is now gone).
		if window := ws.Windows.Pop(ev.Window); window != nil {
			return
		}
	}

	// Drop all queued events for this window.
	for wm.EvQueue.PopBy(ev.Window) != nil {
	}
}

func onEnterNotify(wm *WM, ev *xproto.EnterNotifyEvent) {
	// We only care about normal / ungrab events
	if ev.Mode != xproto.NotifyModeNormal &&
		ev.Mode != xproto.NotifyModeUngrab {
		return
	}

	// We should only received from tracked child windows
	if window := wm.Desktop.Get().Windows.Get(ev.Event); window != nil {
		if err := xproto.SetInputFocusUnchecked(
			wm.XConn,
			xproto.InputFocusPointerRoot,
			ev.Event,
			xproto.TimeCurrentTime,
		); err != nil {
			log.Errorf("error setting input focus: %v", err)
		}
	}
}

func onMotionNotify(wm *WM, ev *xproto.MotionNotifyEvent) {
	if id := wm.Selected; id != xproto.WindowNone {
		// We only care if a window is selected

		// Calculate dx, dy compared to last
		dx := int32(ev.RootX) - wm.LastMouseX
		dy := int32(ev.RootY) - wm.LastMouseY

		// Set new last mouse positions
		wm.LastMouseX = int32(ev.RootX)
		wm.LastMouseY = int32(ev.RootY)

		// Get the selected window, this should be focused already
		if selected := wm.Desktop.Get().Windows.Get(id); selected != nil {
			switch wm.MouseMode {
			// selected is being moved
			case MouseModeMove:
				if err := selected.Move(wm, dx, dy); err != nil {
					log.Errorf("error moving window: %v", err)
				}

			// selected is being resized
			case MouseModeResize:
				if err := selected.Resize(wm, dx, dy); err != nil {
					log.Errorf("error resizing window: %v", err)
				}

			// unhandled
			default:
				log.Panicf("unhandled mouse mode: %d", wm.MouseMode)
			}
		}
	}
}

func onButtonPress(wm *WM, ev *xproto.ButtonPressEvent) {
	if ev.Child == xproto.WindowNone {
		return // no window provided
	}

	// Ensure child window is focused.
	wm.Desktop.Get().Focus(wm, ev.Child)

	// Set the selected window.
	wm.Selected = ev.Child

	// Set current mouse pos.
	wm.LastMouseX = int32(ev.RootX)
	wm.LastMouseY = int32(ev.RootY)

	// Start grabbing X pointer
	if err := xproto.GrabPointerUnchecked(
		wm.XConn,
		false,
		wm.Desktop.Screen.ID,
		xproto.EventMaskButtonRelease|xproto.EventMaskButtonMotion,
		xproto.GrabModeAsync,
		xproto.GrabModeAsync,
		xproto.WindowNone,
		xproto.CursorNone,
		xproto.TimeCurrentTime,
	); err != nil {
		log.Errorf("error grabbing pointer: %v", err)
		return
	}

	// Get mousebutton for event
	switch ev.Detail {
	case xproto.ButtonIndex1:
		wm.MouseMode = MouseModeMove
	case xproto.ButtonIndex3:
		wm.MouseMode = MouseModeResize
	default:
		log.Panicf("unhandled button press: %d", ev.Detail)
	}
}

func onButtonRelease(wm *WM) {
	// Unselect the window + mouse mode
	wm.MouseMode = MouseModeGround
	wm.Selected = xproto.WindowNone

	// Ungrab X pointer now button released
	if err := xproto.UngrabPointerUnchecked(
		wm.XConn,
		xproto.TimeCurrentTime,
	); err != nil {
		log.Errorf("error ungrabbing pointer: %v", err)
	}
}

func onKeyPress(wm *WM, ev *xproto.KeyPressEvent) {
	// Lookup the X keysym for given keycode
	keysym := wm.KBMap.GetKeysym(ev.Detail)

	// Try get function for keybind
	for i := range wm.Config.KeyBinds {
		kb := &wm.Config.KeyBinds[i]

		// Check for matching mod mask and primary key.
		if kb.Mods == ev.State && kb.Key == keysym {
			var focused *Window

			if ev.Child != xproto.WindowNone {
				// Get current workspace
				ws := wm.Desktop.Get()

				// Focus the event window ID.
				ok, err := ws.Focus(wm, ev.Child)
				if err != nil {
					log.Errorf("error focusing window: %v", err)
				}

				if ok {
					// Set the focused window
					focused = ws.Windows.Front()
				}
			}

			// Execute keybind function, passing focused window.
			if err := kb.Func(wm, focused); err != nil {
				log.Errorf("error during keybind func: %v", err)
			}

			return
		}
	}
}

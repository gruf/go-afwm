package wm

import (
	"codeberg.org/gruf/go-logger/v4/log"
	"codeberg.org/gruf/go-xgb/xproto"
)

type Desktop struct {
	// Current workspace index.
	Current int

	// Workspaces part of this desktop.
	Workspaces [9]Workspace

	// Screen is the root X window.
	Screen Window
}

// Get returns currently selected workspace.
func (d *Desktop) Get() *Workspace {
	return &d.Workspaces[d.Current]
}

// GetIdx returns workspace at supplied index.
func (d *Desktop) GetIdx(idx int) *Workspace {
	return &d.Workspaces[idx]
}

// GoTo will change the current workspace index to given, and toggle layout to redraw.
func (d *Desktop) GoTo(wm *WM, idx int) {
	// Get ptr to old workspace
	old := wm.Desktop.Current
	oldWs := &d.Workspaces[old]

	// update to new provided index
	wm.Desktop.Current = idx
	newWs := &d.Workspaces[idx]

	if err := oldWs.Deactivate(wm); err != nil {
		log.Errorf("error deactivating workspace: %v", err)
	}

	if err := newWs.Activate(wm); err != nil {
		log.Errorf("error activating workspace: %v", err)
	}
}

// SendToWorkspace will send the current focused window to the workspace at index.
func (d *Desktop) SendToWorkspace(wm *WM, idx int) error {
	if idx == d.Current {
		// already on ws
		return nil
	}

	if focused := d.Get().Windows.Front(); focused != nil {
		focused := *focused // take our own copy

		// Delete window from current workspace
		_, err := d.Get().Delete(wm, focused.ID)
		if err != nil {
			return err
		}

		// Unmap the window from display.
		if err := xproto.UnmapWindowUnchecked(
			wm.XConn,
			focused.ID,
		); err != nil {
			return wrapErr("error unmapping window", err)
		}

		// Add window to indexed workspace
		if err := d.GetIdx(idx).Add(wm, focused); err != nil {
			return wrapErr("error adding window", err)
		}

		return nil
	}

	return nil
}

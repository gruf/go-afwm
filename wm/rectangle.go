package wm

type Coord struct {
	// X coord in pixels from top-left (0,0).
	X int32

	// Y coord in pixels from top-left (0,0).
	Y int32
}

type Rectangle struct {
	Coord

	// Width in pixels from (X,Y).
	Width uint32

	// Height in pixels from (X,Y).
	Height uint32
}

// func (r *Rectangle) Intersects(o *Rectangle) []Coord {
// 	rXMax := r.X_Max()
// 	rYMax := r.Y_Max()

// 	oXMax := o.X_Max()
// 	oYMax := o.Y_Max()

// 	switch {
// 	case r.X < oXMax:
// 		return nil
// 	case rXMax < o.X:
// 		return nil
// 	case r.Y < oYMax:
// 		return nil
// 	case rYMax < o.Y:
// 		return nil
// 	}

// 	if r.X == o.X {

// 	}

// 	if r.Y == o.Y {

// 	}
// }

func (r *Rectangle) X_Max() int32 {
	return r.X + int32(r.Width)
}

func (r *Rectangle) Y_Max() int32 {
	return r.Y + int32(r.Height)
}

func (r *Rectangle) Size() uint64 {
	return uint64(r.Width) * uint64(r.Height)
}

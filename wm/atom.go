package wm

import (
	"strings"

	"codeberg.org/gruf/go-xgb"
	"codeberg.org/gruf/go-xgb/xproto"
)

type Atom struct {
	// Atom is the numeric atom ID
	Atom xproto.Atom

	// Name is the string representation of this atom
	Name string
}

// Intern fetches this atom with name's ID from X and sets this along with the name.
func (a *Atom) Intern(conn *xgb.XConn, name string) error {
	reply, err := xproto.InternAtom(conn, false, uint16(len(name)), name)
	if err != nil {
		return wrapErr("error interning atom '"+name+"'", err)
	}
	a.Atom = reply.Atom
	a.Name = trimAtom(name)
	return nil
}

// InternedAtoms is a structure of required interned atoms.
type InternedAtoms struct {
	Atom                     Atom
	Supported                Atom
	WMDeleteWindow           Atom
	WMProtocols              Atom
	WMWindowType             Atom
	WMWindowTypeNormal       Atom
	WMWindowTypeDialog       Atom
	WMWindowTypeToolbar      Atom
	WMWindowTypeMenu         Atom
	WMWindowTypePopupMenu    Atom
	WMWindowTypeDropdownMenu Atom
	WMWindowTypeUtility      Atom
	WMWindowTypeSplash       Atom
	WMState                  Atom
}

// InternAll interns all of the atoms contained within InternedAtoms.
func (ia *InternedAtoms) InternAll(wm *WM) error {
	// Set initial atom names
	ia.WMDeleteWindow.Name = "WM_DELETE_WINDOW"
	ia.WMProtocols.Name = "WM_PROTOCOLS"
	ia.WMWindowType.Name = "_NET_WM_WINDOW_TYPE"
	ia.WMWindowTypeNormal.Name = "_NET_WM_WINDOW_TYPE_NORMAL"
	ia.WMWindowTypeDialog.Name = "_NET_WM_WINDOW_TYPE_DIALOG"
	ia.WMWindowTypeToolbar.Name = "_NET_WM_WINDOW_TYPE_TOOLBAR"
	ia.WMWindowTypeMenu.Name = "_NET_WM_WINDOW_TYPE_MENU"
	ia.WMWindowTypePopupMenu.Name = "_NET_WM_WINDOW_TYPE_POPUP_MENU"
	ia.WMWindowTypeDropdownMenu.Name = "_NET_WM_WINDOW_TYPE_DROPDOWN_MENU"
	ia.WMWindowTypeUtility.Name = "_NET_WM_WINDOW_TYPE_UTILITY"
	ia.WMWindowTypeSplash.Name = "_NET_WM_WINDOW_TYPE_SPLASH"
	ia.WMState.Name = "WM_STATE"

	// Intern all atoms from X
	for _, atom := range []*Atom{
		&ia.WMDeleteWindow,
		&ia.WMProtocols,
		&ia.WMWindowType,
		&ia.WMWindowTypeNormal,
		&ia.WMWindowTypeDialog,
		&ia.WMWindowTypeToolbar,
		&ia.WMWindowTypeMenu,
		&ia.WMWindowTypePopupMenu,
		&ia.WMWindowTypeDropdownMenu,
		&ia.WMWindowTypeUtility,
		&ia.WMWindowTypeSplash,
		&ia.WMState,
	} {
		if err := atom.Intern(wm.XConn, atom.Name); err != nil {
			return err
		}
	}

	return nil
}

// trimAtom trims an atom name to remove the leading '_NET_' which is a new XDG design.
func trimAtom(s string) string {
	return strings.TrimPrefix(s, "_NET_")
}
